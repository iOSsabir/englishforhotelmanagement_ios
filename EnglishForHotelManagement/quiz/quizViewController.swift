
//  quizViewController.swift
//
//  Created by sdmgap3 on 3/15/20.
//  Copyright © 2020 Shahid Sabir. All rights reserved.
//

import UIKit

class quizViewController: UIViewController {
    
    @IBOutlet weak var quesionText: UILabel!
    @IBOutlet weak var answer1: UIButton!
    @IBOutlet weak var answer2: UIButton!
    @IBOutlet weak var answer3: UIButton!
    @IBOutlet weak var answer4: UIButton!
    @IBOutlet weak var result: UILabel!
    @IBOutlet weak var nextBTN: UIButton!
    @IBOutlet weak var progressView: UIProgressView!
    @IBOutlet weak var progressLB: UILabel!
    @IBOutlet weak var viewshadow: UIView!    
    @IBOutlet weak var viewdesign: UIView!
    
    let progress = Progress(totalUnitCount: 10)
    var minValue = 0
    var maxValue = 100
    
    var questionNumber : UInt32!
    

    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        viewshadow.layer.shadowColor = UIColor.black.cgColor
        viewshadow.layer.shadowOpacity = 1
        viewshadow.layer.shadowOffset = .zero
        viewshadow.layer.shadowRadius = 10
        
        viewdesign.layer.cornerRadius = 25
        
        answer1.layer.borderWidth = 2
        answer1.layer.borderColor = UIColor.blue.cgColor
        answer1.layer.cornerRadius = 10
        
        answer2.layer.borderWidth = 2
        answer2.layer.borderColor = UIColor.blue.cgColor
        answer2.layer.cornerRadius = 10
        
        answer3.layer.borderWidth = 2
        answer3.layer.borderColor = UIColor.blue.cgColor
        answer3.layer.cornerRadius = 10
        
        answer4.layer.borderWidth = 2
        answer4.layer.borderColor = UIColor.blue.cgColor
        answer4.layer.cornerRadius = 10
        
        progressView.setProgress(Float(minValue), animated: true)
        progressView.transform = progressView.transform.scaledBy(x: 1, y: 5)
        
        result.isHidden = true
        self.chooseQuestion()
        //self.clockProgress()
 
    }
    
    @IBAction func answer1Action(_ sender: Any) {
        
        answer2.isEnabled = false
        answer3.isEnabled = false
        answer4.isEnabled = false
        
        result.isHidden = false

        if questionNumber == 0 {
            result.text = "CORRECT"
        }else if questionNumber == 1{
            result.text = "WRONG"
        }else if questionNumber == 2{
            result.text = "WRONG"
        }else if questionNumber == 3{
            result.text = "WRONG"

        }else if questionNumber == 4{
            result.text = "WRONG"

        }else if questionNumber == 5{
            result.text = "WRONG"
          

        }
        else if questionNumber == 6{
            result.text = "CORRECT"
        }
        
        else if questionNumber == 7{
            result.text = "WRONG"

        }
        
        else if questionNumber == 8{
            result.text = "WRONG"

        }
        
        else if questionNumber == 9{
            result.text = "WRONG"

        }
        
    }
    
    
    @IBAction func answer2Action(_ sender: Any) {
        
        answer1.isEnabled = false
        answer3.isEnabled = false
        answer4.isEnabled = false
        
        result.isHidden = false

        if questionNumber == 0 {
            result.text = "WRONG"
        }else if questionNumber == 1{
            result.text = "CORRECT"

        }else if questionNumber == 2{
            result.text = "WRONG"
        }else if questionNumber == 3{
            result.text = "WRONG"

        }else if questionNumber == 4{
            result.text = "WRONG"

        }else if questionNumber == 5{
            result.text = "CORRECT"

        }
        else if questionNumber == 6{
            result.text = "WRONG"

        }
        
        else if questionNumber == 7{
            result.text = "CORRECT"

        }
        
        else if questionNumber == 8{
            result.text = "WRONG"

        }
        
        else if questionNumber == 9{
            result.text = "WRONG"

        }
        
    }
    
    
    @IBAction func answer3Action(_ sender: Any) {
        
        answer2.isEnabled = false
        answer1.isEnabled = false
        answer4.isEnabled = false
        
        result.isHidden = false

        if questionNumber == 0 {
            result.text = "WRONG"
        }else if questionNumber == 1{
            result.text = "WRONG"
        }else if questionNumber == 2{
            result.text = "CORRECT"
            
            
        }else if questionNumber == 3{
            result.text = "WRONG"

        }else if questionNumber == 4{
            result.text = "CORRECT"

        }else if questionNumber == 5{
            result.text = "WRONG"

        }
        
        else if questionNumber == 6{
            result.text = "WRONG"

        }
        
        else if questionNumber == 7{
            result.text = "WRONG"

        }
        
        else if questionNumber == 8{
            result.text = "CORRECT"

        }
        
        else if questionNumber == 9{
            result.text = "WRONG"

        }
        
    }
    
    
    @IBAction func answer4Action(_ sender: Any) {
        
        answer2.isEnabled = false
        answer3.isEnabled = false
        answer1.isEnabled = false
        
        result.isHidden = false

        if questionNumber == 0 {
            result.text = "WRONG"
        }else if questionNumber == 1{
            result.text = "WRONG"
        }else if questionNumber == 2{
            result.text = "WRONG"
        }else if questionNumber == 3{
            result.text = "CORRECT"

        }else if questionNumber == 4{
            result.text = "WRONG"

        }else if questionNumber == 5{
            result.text = "WRONG"

        }
        
        else if questionNumber == 6{
            result.text = "WRONG"

        }
        
        else if questionNumber == 7{
            result.text = "WRONG"

        }
        
        else if questionNumber == 8{
            result.text = "WRONG"

        }
        
        else if questionNumber == 9{
            result.text = "CORRECT"

        }
        
    }
    
    
    @IBAction func nextAction(_ sender: Any) {
        
//        self.progress.completedUnitCount += 1
//        let progressFloat = Float(self.progress.fractionCompleted)
//        self.progressView.setProgress(progressFloat, animated: true)
    
        answer1.isEnabled = true
        answer2.isEnabled = true
        answer3.isEnabled = true
        answer4.isEnabled = true
        
        result.isHidden = true

        progressViewAnim()
        self.chooseQuestion()
        
        
    }
    
    func progressViewAnim(){
        self.progressView.progress = 0
        Timer.scheduledTimer(withTimeInterval: 3, repeats: true) { (timer) in
            guard self.progress.isFinished == false else {
                timer.invalidate()
                print("Finished")
                return
            }
        self.progress.completedUnitCount += 1
        let progressFloat = Float(self.progress.fractionCompleted)
        self.progressView.setProgress(progressFloat, animated: true)
            //self.progressView.progress = progressFloat
            
        }
    }
    
    func chooseQuestion () {
        questionNumber = arc4random() % 10
        
        switch questionNumber {
            case 0:
            quesionText.text = "why do you want to work at this hotel? Do you have any experience working in a hotel in bangladesh"
            answer1.setTitle("Yes I have", for: UIControl.State.normal)
            answer2.setTitle("No I did not", for: UIControl.State.normal)
            answer3.setTitle("A little bit", for: UIControl.State.normal)
            answer4.setTitle("For 1 year", for: UIControl.State.normal)
            
            case 1:
            quesionText.text = "1+1="
            answer1.setTitle("3", for: UIControl.State.normal)
            answer2.setTitle("2", for: UIControl.State.normal)
            answer3.setTitle("5", for: UIControl.State.normal)
            answer4.setTitle("7", for: UIControl.State.normal)
            
            case 2:
            quesionText.text = "2+7="
            answer1.setTitle("3", for: UIControl.State.normal)
            answer2.setTitle("6", for: UIControl.State.normal)
            answer3.setTitle("9", for: UIControl.State.normal)
            answer4.setTitle("5", for: UIControl.State.normal)
            
            case 3:
            quesionText.text = "what is my name"
            answer1.setTitle("David", for: UIControl.State.normal)
            answer2.setTitle("Adam", for: UIControl.State.normal)
            answer3.setTitle("Shahid", for: UIControl.State.normal)
            answer4.setTitle("Sabir", for: UIControl.State.normal)

            case 4:
            quesionText.text = "what do you do when a bully comes?"
            answer1.setTitle("foght", for: UIControl.State.normal)
            answer2.setTitle("cry", for: UIControl.State.normal)
            answer3.setTitle("talk", for: UIControl.State.normal)
            answer4.setTitle("moon of the above", for: UIControl.State.normal)
            
            case 5:
            quesionText.text = "who relessed the mac"
            answer1.setTitle("microsoft", for: UIControl.State.normal)
            answer2.setTitle("apple", for: UIControl.State.normal)
            answer3.setTitle("stanford", for: UIControl.State.normal)
            answer4.setTitle("google", for: UIControl.State.normal)
            
            case 6:
            quesionText.text = "What is a concierge?"
            answer1.setTitle("Helping guests book tours and trips", for: UIControl.State.normal)
            answer2.setTitle("Manager", for: UIControl.State.normal)
            answer3.setTitle("Room Cleaner", for: UIControl.State.normal)
            answer4.setTitle("Chef", for: UIControl.State.normal)
            
            case 7:
            quesionText.text = "How would you feel about working night shifts?"
            answer1.setTitle("Bad", for: UIControl.State.normal)
            answer2.setTitle("Good", for: UIControl.State.normal)
            answer3.setTitle("Impossible", for: UIControl.State.normal)
            answer4.setTitle("Not interested", for: UIControl.State.normal)
            
            case 8:
            quesionText.text = "Where do you see yourself in five years?"
            answer1.setTitle("Same position", for: UIControl.State.normal)
            answer2.setTitle("To be a owner of a hotel", for: UIControl.State.normal)
            answer3.setTitle("Manager", for: UIControl.State.normal)
            answer4.setTitle("Not interested to stay for five years", for: UIControl.State.normal)
            
            case 9:
            quesionText.text = "Which manager is considered to be the best as per your knowledge?"
            answer1.setTitle("Old Aged", for: UIControl.State.normal)
            answer2.setTitle("Smart Looking", for: UIControl.State.normal)
            answer3.setTitle("Angry man", for: UIControl.State.normal)
            answer4.setTitle("Well mannerd person", for: UIControl.State.normal)
            
            
        default:
            break
        }
    }
    
    @IBAction func progressAction(_ sender: Any) {
                    progressViewAnim()
            }
    
        let shapeLayer = CAShapeLayer()
        func clockProgress(){
       
        let center = view.center
            //Create my track layer
        let trackLayer = CAShapeLayer()
        let circularPath = UIBezierPath(arcCenter: center, radius: 100, startAngle:  -CGFloat.pi / 2, endAngle: 2 * CGFloat.pi, clockwise: true)
        trackLayer.path = circularPath.cgPath
        trackLayer.strokeColor = UIColor.lightGray.cgColor
        trackLayer.lineWidth = 10
        trackLayer.fillColor = UIColor.clear.cgColor
        trackLayer.lineCap = CAShapeLayerLineCap.round
        view.layer.addSublayer(trackLayer)
            
        //let circularPath = UIBezierPath(arcCenter: center, radius: 100, startAngle:  -CGFloat.pi / 2, endAngle: 2 * CGFloat.pi, clockwise: true)
        shapeLayer.path = circularPath.cgPath
        shapeLayer.strokeColor = UIColor.red.cgColor
        shapeLayer.lineWidth = 10
        shapeLayer.fillColor = UIColor.clear.cgColor
        shapeLayer.lineCap = CAShapeLayerLineCap.round
        shapeLayer.strokeEnd = 0
        view.layer.addSublayer(shapeLayer)
        view.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(handleTap)))
    }
   @objc private func handleTap(){
        print("Attempting to animate application")
        let basicAnimation = CABasicAnimation(keyPath: "strokeEnd")
    basicAnimation.toValue = 1
    basicAnimation.duration = 7
    basicAnimation.fillMode = CAMediaTimingFillMode.forwards
    basicAnimation.isRemovedOnCompletion = false
    shapeLayer.add(basicAnimation, forKey: "urSoBasic")
    }
    
    @IBAction func backToLesson(_ sender: Any) {
           dismiss(animated: true, completion: nil)
    }
    
        }
    

